
# ----------------------------------------TIPOS NÚMERICOS---------------------------------------

# enteros
import sys
import os
x = 10
print(x)
print(type(x))  # Salida=<class 'int'>

# flotantes

y = 12.45
print(y)
print(type(y))  # Salida=<class 'float'>

# complejos
i = 1+3j
print(i)
print(type(i))  # Salida=<class 'complex'>

# ----------------------------------------TIPOS DE SEQUENCIA---------------------------------------

# cadenas
bienvenido = "Hola bienvenido al mundo de python"
print(bienvenido)
print(type(bienvenido))

# tuplas
tupla = (1, 3, 4, 5, 'r')  # una tupla es inmutable... no se puede modificar
print(tupla)
print(tupla[2])
print(type(tupla))


# listas
# una lista es mutable...se puede modificar
lista = [1, 2, 4, 4, 'd', 2, 5.667]
print(lista)
print(lista[4])
print(type(lista))

# ----------------------------------------TIPO DICTIONARY---------------------------------------

dictionary = {1: 'a', 2: 'b'}
print(dictionary)
print("Posición 2: ", dictionary[2])
print(type(dictionary))


# ----------------------------------------TIPOS BOOLEANOS---------------------------------------

verdadero = True
falso = False
print(verdadero)
print(falso)
print(type(verdadero))  # Salida=<class 'bool'>
print(type(falso))  # Salida=<class 'bool'>

evaluar = 1 > 3
print(evaluar)  # False


# ----------------------------------------TIPO SET-----------------------------------------------

sett = {1, 2, 3, 4}
print(sett)
print(type(sett))  # Salida=<class 'set'>
